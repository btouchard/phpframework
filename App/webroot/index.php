<?php
if (!defined('APP_DIR')) {
    define('APP_DIR', dirname(dirname(dirname(__FILE__))) . '\\App');
}
if (!defined('LIB_DIR')) {
    define('LIB_DIR', dirname(dirname(dirname(__FILE__))) . '\\Library');
}

require_once LIB_DIR . '/bootstrap.php';

//$site = new \App\Site();
$site = new \App\Site();
$site->run();