<!DOCTYPE html>
<html>
<head>
    <?=$this->Html->meta(array('charset'=>'utf-8'))?>
    <?=$this->Html->title($title)?>
    <?=$this->Html->meta(array('name'=>'viewport', 'content'=>'width=device-width, initial-scale=1.0'))?>
    <?=$this->Html->meta(array('name'=>'author', 'content'=>'Benjamin Touchard'))?>
    <?=$this->Html->meta(array('name'=>'description', 'content'=>'Votre Freebox Révolution/HD et la Messagerie Vocale Visuelle FreeMobile sur Android'))?>
    <?=$this->Html->script('http://code.jquery.com/jquery-2.0.3.min.js')?>
    <?=$this->Html->script('user.js')?>
    <?=$this->Html->css('bootstrap/bootstrap.css')?>
    <?=$this->Html->css('bootstrap/bootstrap-responsive.css')?>
    <?=$this->Html->css('style')?>
</head>

<body>
    <?=$this->Html->header($header)?>
    <?/*=$this->Html->br()?>
    <?=$this->Html->input(array('name'=>'login','value'=>''))?>
    <?=$this->Html->input(array('name'=>'password','type'=>'password','value'=>''))*/?>
    <div><?=$content?></div>
    <?=$this->Html->footer($footer)?>
</body>
</html>