<?php
namespace Library;

use Library\Utils\Debug;

class HTTPRequest {

    private $uri;
    private $rest = false;
    private $method, $accept, $referer;
    private $module = null, $action = null;

    public function __construct() {
        $this->uri = $_SERVER['REQUEST_URI'];
        if (preg_match('#^' . Application::SEPARATOR . ROOT_NAME.'#', $_SERVER['REQUEST_URI']) > 0) {
            define('BASE_URL', Application::SEPARATOR . ROOT_NAME . Application::SEPARATOR);
            $this->uri = str_replace(BASE_URL, '', $this->uri);
        } else define('BASE_URL', Application::SEPARATOR);
        $this->uri = str_replace('?' . $_SERVER['QUERY_STRING'], '', $this->uri);
        $this->uri = preg_replace('#.html?#', '', $this->uri);
        $this->referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
        $this->accept = $_SERVER['HTTP_ACCEPT'];
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->rest = (bool) !empty($this->accept) && strpos($this->accept, 'application/json') > -1;
    }

    public function html() {
        return !$this->rest;
    }
    public function rest($bool = null) {
        if ($bool !== null) $this->rest = $bool;
        return $this->rest;
    }
    public function module($module = null) {
        if ($module !== null) $this->module = $module;
        return $this->module;
    }
    public function action($action = null) {
        if ($action !== null) $this->action = $action;
        return $this->action;
    }

    public function method() {
        return $this->method;
    }
    public function accept() {
        return $this->accept;
    }
    public function referer() {
        return $this->referer;
    }
    public function requestURI() {
        return $this->uri;
    }
}