<?php
namespace Library;

use Library\Utils\DB;
use Library\Utils\Debug;

class RouterException extends \Exception {}

class Router extends Component {

    const DEFAULT_MODULE = 'Home';
    const DEFAULT_ACTION_LIST = 'index';
    const DEFAULT_ACTION_SHOW = 'show';

    public function getController() {
        $this->setupModule();
        $this->checkData();
        $class = $this->getModule();
        return new $class($this->app);
    }

    private function getModule() {
        return '\\App\\Controller\\' . $this->app->request()->module() . 'Controller';
    }

    private function setupModule() {
        $url = trim($this->app->request()->requestURI(), '/');
        $params = array();
        $this->app->request()->action(self::DEFAULT_ACTION_LIST);
        if (empty($url)) {
            $this->app->request()->module(self::DEFAULT_MODULE);
        } else {
            $params = explode(Application::SEPARATOR, $url);
            if (count($params) > 0 && is_string($params[0])) {
                $this->app->request()->module(ucwords(array_shift($params)));
            }
        }
        $class = $this->getModule();
        //Debug::log($class);
        //Debug::log(exist($class));
        if (!exist($class)) throw new RouterException('Le controller demandé \'' . $this->module . '\' n\'existe pas !', 404);
        if (count($params) > 0 && is_numeric($params[0])) {
            $_GET['id'] = (int) array_shift($params);
            $this->app->request()->action(self::DEFAULT_ACTION_SHOW);
        }
        if (count($params) > 0 && is_string($params[0]) && preg_match('#^[a-zA-Z_]+$#', $params[0]) > 0) {
            $a = (string) $params[0];
            $i = new $class($this->app, null);
            if (method_exists($i, $a)) {
                $this->app->request()->action($a);
                array_shift($params);
            } else {
                $reg = '#^([a-z]+)_([a-z]+)$#';
                if (preg_match($reg, $a, $match) > 0) {
                    $a = $match[1] . ucwords($match[2]);
                    if (method_exists($i, $a)) {
                        $this->app->request()->action($a);
                        array_shift($params);
                    }
                    //else throw new RouterException('La méthode demandée \'' . $this->action . '\' n\'existe pas !', 404);
                }// else
                //   throw new RouterException('La méthode demandée \'' . $this->action . '\' n\'existe pas !', 404);
            }
        }
        if (count($params) > 0) {
            for ($i=0; $i<count($params); $i+=2) {
                if (isset($params[$i]) && !is_null($params[$i]) && is_string($params[$i])) {
                    $_GET[$params[$i]] = isset($params[$i+1]) && !is_null($params[$i+1]) ? $params[$i+1] : null;
                }
            }
        }
    }

    /*private function checkDBModule($module) {
        $qry = 'SHOW TABLES WHERE `Tables_in_mydb` LIKE  \'' . $module . '\'';
        $rs = DB::query($qry);
        if ($rs->rowCount() > 0) {
            $this->module = $module;
            $this->class = '\\Library\\Controller\\Controller';
            $this->dynamic = true;
        } else {
            $this->module = null;
        }
    }*/

    private function checkData() {
        switch ($this->app->request()->method()) {
            case 'POST':
                if ($this->app->request()->rest()) {
                    $data = (array) json_decode(file_get_contents('php://input'));
                    if (count($data) > 0) $_POST = array_merge($_POST, $data);
                }
                break;
            case 'PUT':
                if ($this->app->request()->rest()) {
                    parse_str(file_get_contents("php://input"), $input);
                    if (count($input) > 0) $_FILES = array_merge($_FILES, $input);
                }
                break;
        }
    }
}